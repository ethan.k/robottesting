*** Settings ***
Library    SeleniumLibrary
Library    cumulusci.robotframework.CumulusCI
Library    cumulusci.robotframework.Salesforce
variables    variables.py

*** Test Cases ***
Create/Delete Account
    Login
    Verify Login
    Create New Account
    Teardown




*** Keywords ***
Login
    Open Browser    https://login.salesforce.com/    chrome
    Input Text    id:username    ${USERNAME}
    Input Text    id:password    ${PASSWORD}
    Click Element    Login

Verify Login   
    Wait Until Page Contains    Home    timeout=20

Teardown
    Close Browser

Create New Account
    Go To Object Home    obj_Accounts
    Wait For Aura
    Click Object Button    New

